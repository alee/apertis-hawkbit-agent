/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AHA_HAWKBIT_CONFIG_DATA_H__
#define __AHA_HAWKBIT_CONFIG_DATA_H__

#include <glib-object.h>
#include <glib.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define AHA_TYPE_HAWKBIT_CONFIG_DATA aha_hawkbit_config_data_get_type ()

G_DECLARE_FINAL_TYPE (AhaHawkbitConfigData,
                      aha_hawkbit_config_data,
                      AHA,
                      HAWKBIT_CONFIG_DATA,
                      GObject)

void
aha_hawkbit_config_data_send (AhaHawkbitConfigData *self);

G_END_DECLS

#endif /* __AHA_HAWKBIT_CONFIG_DATA_H__ */
