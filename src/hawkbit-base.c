/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include "hawkbit-base.h"
#include "hawkbit-session.h"

#include <json-glib/json-glib.h>

struct _AhaHawkbitBase{
  GObject parent;
  AhaHawkbitSession *session;
  JsonParser *parser;
};

enum
{
  PROP_HAWKBIT_SESSION = 1,
  NUM_PROPERTIES
};
static GParamSpec *props[NUM_PROPERTIES] = { NULL, };


G_DEFINE_TYPE (AhaHawkbitBase,
               aha_hawkbit_base,
               G_TYPE_OBJECT)

static void
aha_hawkbit_base_init (AhaHawkbitBase * self)
{
}

/*
static void
aha_hawkbit_base_constructed (GObject *object)
{
  //AhaHawkbitBase *self = AHA_HAWKBIT_BASE (object);
}
*/

static void
aha_hawkbit_base_dispose (GObject * object)
{
  AhaHawkbitBase *self = AHA_HAWKBIT_BASE (object);

  g_clear_object (&self->session);
  g_clear_object (&self->parser);

  if (G_OBJECT_CLASS (aha_hawkbit_base_parent_class)->dispose)
    G_OBJECT_CLASS (aha_hawkbit_base_parent_class)->dispose (object);
}


static void
aha_hawkbit_base_set_property (GObject *object,
                      guint prop_id,
                      const GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitBase *self = AHA_HAWKBIT_BASE (object);

  switch (prop_id)
    {
      case PROP_HAWKBIT_SESSION:
        self->session = g_value_dup_object (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_base_get_property (GObject *object,
                      guint prop_id,
                      GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitBase *self = AHA_HAWKBIT_BASE (object);

  switch (prop_id)
    {
      case PROP_HAWKBIT_SESSION:
        g_value_set_object (value, self->session);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_base_class_init (AhaHawkbitBaseClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  props[PROP_HAWKBIT_SESSION] =
    g_param_spec_object ("session",
                         "Hawkbit Session",
                         "Hawkbit Session Object",
                         AHA_TYPE_HAWKBIT_SESSION,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  object_class->dispose = aha_hawkbit_base_dispose;
  object_class->get_property = aha_hawkbit_base_get_property;
  object_class->set_property = aha_hawkbit_base_set_property;
  //object_class->constructed = aha_hawkbit_base_constructed;

  g_object_class_install_properties (object_class,
                                     NUM_PROPERTIES,
                                     props);
}

gboolean
aha_hawkbit_base_update (AhaHawkbitBase *self)
{
  g_autoptr  (SoupMessage) msg;
  guint status;

  g_message ("Retrieving base update info");

  g_clear_object (&self->parser);

  msg = aha_hawkbit_session_create_message (self->session,
                                            SOUP_METHOD_GET,
                                            "");
  status = soup_session_send_message (
    aha_hawkbit_session_get_soup_session (self->session),
    msg);
  if (status < 200 || status > 299)
    {
      g_debug ("%s: status=%d message='%s'", __func__, status,
                                            msg->response_body->data);
      return FALSE;
    }

  self->parser = json_parser_new();

  json_parser_load_from_data (self->parser,
                              msg->response_body->data,
                              msg->response_body->length,
                              NULL);

  return TRUE;
}

static gchar *
get_uri(AhaHawkbitBase *self, gchar* member)
{
  JsonNode *root = NULL;
  JsonNode *node = NULL;
  JsonObject *obj = NULL;

  if (self->parser == NULL)
    return NULL;

  root = json_parser_get_root (self->parser);
  obj = json_node_get_object (root);
  node = json_object_get_member (obj, "_links");
  if (node == NULL)
    return NULL;

  obj = json_node_get_object (node);
  node = json_object_get_member (obj, member);
  if (node == NULL)
    return NULL;

  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "href");

  return json_node_dup_string (node);
}

gchar *
aha_hawkbit_base_get_deployment_base (AhaHawkbitBase *self)
{
  return get_uri (self, "deploymentBase");
}

gchar *
aha_hawkbit_base_get_config_data (AhaHawkbitBase *self)
{
  return get_uri (self, "configData");
}

int
aha_hawkbit_base_get_polling_sleep (AhaHawkbitBase *self)
{
  JsonNode *root = NULL;
  JsonNode *node = NULL;
  JsonObject *obj = NULL;
  const gchar *sleep_str;
  gchar **sleep_str_array, **ptr;
  int sleep_time = 0;

  if (self->parser == NULL)
    return -1;

  root = json_parser_get_root (self->parser);
  obj = json_node_get_object (root);
  node = json_object_get_member (obj, "config");
  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "polling");
  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "sleep");
  sleep_str = json_node_get_string(node);

  if (sleep_str == NULL)
    return -1;

  sleep_str_array = g_strsplit(sleep_str, ":", 3);
  for (ptr = sleep_str_array; *ptr; ptr++)
    sleep_time = (sleep_time * 60) + atoi(*ptr);

  g_strfreev(sleep_str_array);

  return sleep_time;
}
