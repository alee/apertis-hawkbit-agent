Apertis hawkBit Agent
=====================

The apertis-hawkbit-agent is a component in the OSTree-based images to poll
hawkBit update server, and in case of an update is available to download it
and trigger apertis-update-manager to apply it.

The daemon
==========

The daemon is started and stopped using systemd.
The typical boot sequence is similar to the log below:

    ● apertis-hawkbit-agent.service - Apertis hawkBit agent
       Loaded: loaded (/lib/systemd/system/apertis-hawkbit-agent.service; enabled; vendor preset: enabled)
       Active: active (running) since Wed 2019-10-09 06:27:19 UTC; 30min ago
     Main PID: 372 (apertis-hawkbit)
        Tasks: 5 (limit: 2311)
       Memory: 8.5M
       CGroup: /system.slice/apertis-hawkbit-agent.service
               └─372 /usr/lib/apertis-hawkbit-agent/apertis-hawkbit-agent
    
    Oct 09 06:27:19 apertis systemd[1]: Started Apertis hawkBit agent.
    Oct 09 06:57:18 apertis apertis-hawkbit[372]: Retrieving base update info
    Oct 09 06:57:19 apertis apertis-hawkbit[372]: Changing poll timeout to 30 seconds
    Oct 09 06:57:49 apertis apertis-hawkbit[372]: Retrieving base update 

It is run at boot from a dedicated service file requested by `multi-user.target`.

Getting logs
============

    $ journalctl --unit apertis-hawkbit-agent
    -- Logs begin at Thu 2019-02-14 10:11:58 UTC, end at Wed 2019-10-09 06:59:49 UTC. --
    Oct 09 06:27:19 apertis systemd[1]: Started Apertis hawkBit agent.
    Oct 09 06:57:18 apertis apertis-hawkbit[372]: Retrieving base update info
    Oct 09 06:57:19 apertis apertis-hawkbit[372]: Changing poll timeout to 30 seconds
    Oct 09 06:57:49 apertis apertis-hawkbit[372]: Retrieving base update info


License
=======

This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
